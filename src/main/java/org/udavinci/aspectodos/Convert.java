package org.udavinci.aspectodos;

import javax.swing.*;

public class Convert {
    public static void main(String args[]) {
        ConvertFrame convertFrame = new ConvertFrame();
        convertFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        convertFrame.setSize(300, 220); // set frame size
        convertFrame.setVisible(true); // display frame
    }
}

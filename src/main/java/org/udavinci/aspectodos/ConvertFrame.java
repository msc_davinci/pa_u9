package org.udavinci.aspectodos;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class ConvertFrame extends JFrame {
    private JPanel fromJPanel;
    private JPanel toJPanel;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JTextField tempJTextField1;
    private JTextField tempJTextField2;
    private ButtonGroup fromButtonGroup;
    private ButtonGroup toButtonGroup;
    private JRadioButton celsiusToJRadioButton;
    private JRadioButton fahrenheitToJRadioButton;
    private JRadioButton celsiusFromJRadioButton;
    private JRadioButton fahrenheitFromJRadioButton;

    public ConvertFrame() {
        super("Conversor de Temperatura");

        // create ButtonGroup for from JRadioButtons
        fahrenheitFromJRadioButton =
                new JRadioButton("Fahrenheit", true);
        celsiusFromJRadioButton = new JRadioButton("Celsius", false);
        fromButtonGroup = new ButtonGroup();
        fromButtonGroup.add(fahrenheitFromJRadioButton);
        fromButtonGroup.add(celsiusFromJRadioButton);
        fahrenheitToJRadioButton =
                new JRadioButton("Fahrenheit", false);
        celsiusToJRadioButton = new JRadioButton("Celsius", true);
        toButtonGroup = new ButtonGroup();
        toButtonGroup.add(fahrenheitToJRadioButton);
        toButtonGroup.add(celsiusToJRadioButton);

        // create from JPanel
        fromJPanel = new JPanel();
        fromJPanel.setLayout(new GridLayout(1, 3));
        fromJPanel.add(fahrenheitFromJRadioButton);
        fromJPanel.add(celsiusFromJRadioButton);

        // create to JPanel
        toJPanel = new JPanel();
        toJPanel.setLayout(new GridLayout(1, 3));
        toJPanel.add(fahrenheitToJRadioButton);
        toJPanel.add(celsiusToJRadioButton);

        // create labels
        label1 = new JLabel("Convertir de:");
        label2 = new JLabel("Convertir a:");
        label3 = new JLabel("Ingresa la Temperatura: ");
        label4 = new JLabel("La Temperature convertida es: ");

        // create JTextField for getting temperature to be converted
        tempJTextField1 = new JTextField(10);
        tempJTextField1.addActionListener(

                new ActionListener() // anonymous inner class
                {
                    // perform conversions
                    public void actionPerformed(ActionEvent event) {
                        int convertTemp, temp;

                        temp = Integer.parseInt(((JTextField)
                                event.getSource()).getText());

                        // fahrenheit to celsius
                        if (fahrenheitFromJRadioButton.isSelected() &&
                                celsiusToJRadioButton.isSelected()) {
                            convertTemp = (int) (5.0f / 9.0f * (temp - 32));
                            tempJTextField2.setText(
                                    String.valueOf(convertTemp));
                        } // end if

                        // celsius to fahrenheit
                        else if (celsiusFromJRadioButton.isSelected() &&
                                fahrenheitToJRadioButton.isSelected()) {
                            convertTemp = (int) (9.0f / 5.0f * temp + 32);
                            tempJTextField2.setText(
                                    String.valueOf(convertTemp));
                        } // end else if
                    } // end method actionPerformed
                } // end anonymous inner class
        ); // end call to addActionListener

        // JTextField to display temperature after convertion
        tempJTextField2 = new JTextField(10);
        tempJTextField2.setEditable(false);

        // add components to GUI
        setLayout(new GridLayout(8, 1));
        add(label1);
        add(fromJPanel);
        add(label3);
        add(tempJTextField1);
        add(label2);
        add(toJPanel);
        add(label4);
        add(tempJTextField2);
    }
}
